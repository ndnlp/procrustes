# Procrustes

This is a Python script that we use to project alignments onto different tokenizations. The usage is:

    procrustes3.py -m align [--english] new-tok < input > output

where

- `new-tok` is the new tokenization of the French side
- `input` is a tab-delimited file with the French, English, and word alignment
    - French and English use old tokenization
    - word alignment is zero-based f-e format

        ceci n' est pas une pipe \t this is not a pipe \t 0-0 1-2 2-1 3-2 4-3 5-4

    - `output` is in the same format as input, but using new tokenization on French side

If you pass the `--english` flag, then the projection will be done on the English side instead of the French side. Thus, you should run it twice, like this:

    cat input | procrustes3.py -m align new-french-tok | procrustes3.py -m align --english new-english-tok > output

The reason for the weird usage is for generality -- it also works on trees and another version worked on POS tags as well.

The way it works is to compute a minimal sequence of string edits to get from the old tokenization to the new tokenization. Then these string edits induce corresponding edit operations on alignments (or trees, or whatever). But the rules aren't very smart -- if the new tokenization splits a token, both new tokens will be aligned. It would be nice to improve this.
